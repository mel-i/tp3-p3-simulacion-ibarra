package visual;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class VentanaInicial extends Ventana {

	private static final long serialVersionUID = 1L;
	private VentanaEstadistica estadisticas;
	private BackgroundPane background;
	private JLabel lblPersonas;
	private JLabel lblSimulaciones;
	private JTextField cantPersonas;
	private JTextField cantSimulaciones;
	private final ButtonGroup buttonGroupIntercambio;
	private JRadioButton intercambio;
	private JRadioButton donaciones;
	private JComboBox<String> comboBox;
	private JButton btnSimular;
	private JLabel preguntaRepetidas;

	public VentanaInicial() {
		super();
		buttonGroupIntercambio = new ButtonGroup();
		estadisticas = new VentanaEstadistica();
		siguienteVentana = estadisticas;
		background = new BackgroundPane();
		crearFrame();
	}

	@Override
	protected void crearFrame() {
		setTitle("Simulacion del Album");
		setBounds(100, 100, 600, 450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ponerFondoDePantalla();
		crearBotonSonido();
		
		comboBox = new JComboBox<>();
		comboBox.setBounds(60, 130, 162, 21);
		getContentPane().add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {
							"Seleccione una opcion", "Unica persona", 
							"Unica Simulacion", "Varias simulaciones"}));
		comboBox.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedIndex() != 0) {
					mostrarConfiguraciones();
				}
			}
			
		});
		
		intercambio = new JRadioButton("intercambiar");
		buttonGroupIntercambio.add(intercambio);
		intercambio.setBounds(60, 299, 103, 21);
		getContentPane().add(intercambio);
		intercambio.setVisible(false);
		
		donaciones = new JRadioButton("donar");
		buttonGroupIntercambio.add(donaciones);
		donaciones.setBounds(60, 264, 103, 21);
		getContentPane().add(donaciones);
		donaciones.setVisible(false);
		
		lblPersonas = new JLabel("Cantidad de personas: ");
		lblPersonas.setForeground(Color.WHITE);
		lblPersonas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPersonas.setBounds(60, 161, 162, 27);
		getContentPane().add(lblPersonas);
		lblPersonas.setVisible(false);
		
		cantPersonas = new JTextField();
		cantPersonas.setBounds(204, 166, 96, 19);
		getContentPane().add(cantPersonas);
		cantPersonas.setColumns(10);
		cantPersonas.setVisible(false);
		
		lblSimulaciones = new JLabel("Cantidad de simulaciones: ");
		lblSimulaciones.setForeground(Color.WHITE);
		lblSimulaciones.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSimulaciones.setBounds(60, 190, 162, 27);
		getContentPane().add(lblSimulaciones);
		lblSimulaciones.setVisible(false);
		
		cantSimulaciones = new JTextField();
		cantSimulaciones.setBounds(204, 195, 96, 19);
		getContentPane().add(cantSimulaciones);
		cantSimulaciones.setColumns(10);
		cantSimulaciones.setVisible(false);
		
		preguntaRepetidas = new JLabel("\u00BFQu\u00E9 desea hacer con las figuritas repetidas?");
		preguntaRepetidas.setForeground(Color.WHITE);
		preguntaRepetidas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		preguntaRepetidas.setBounds(60, 227, 261, 21);
		getContentPane().add(preguntaRepetidas);
		preguntaRepetidas.setVisible(false);
		
		btnSimular = new JButton("Simular");
		btnSimular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				siguienteVentana();
			}
		});
		btnSimular.setBounds(60, 345, 85, 21);
		getContentPane().add(btnSimular);
		btnSimular.setVisible(false);
	}
	

	private boolean datosValidos() {
		Pattern patron = Pattern.compile("[1-9]\\d*");
		Matcher matcher = patron.matcher(cantPersonas.getText());
		Matcher matcher2 = patron.matcher(cantSimulaciones.getText());
		
		if (comboBox.getSelectedIndex() == 1) {
			return matcher2.find();
		}
		
		boolean validez = matcher.find();
		
		if (comboBox.getSelectedIndex() == 2) {
			return validez && (intercambio.isSelected() || donaciones.isSelected());
		}
		
		return validez && matcher2.find() && (intercambio.isSelected() || donaciones.isSelected());
	}

	
	private void siguienteVentana() {
		if (datosValidos()) {
			enviarInformacion();
			cambiarVentana();
		} else {
			JOptionPane.showMessageDialog(getContentPane(), "Los datos no son v�lidos");
		}
	}
	
	private void enviarInformacion() {
		if (comboBox.getSelectedIndex() == 1) {
			estadisticas.iniciarSimulacion(1, Integer.parseInt(cantSimulaciones.getText()), "Simple");
			return;
		} 
		
		if (comboBox.getSelectedIndex() == 2) {
			if (intercambio.isSelected()) {
				estadisticas.iniciarSimulacion(Integer.parseInt(cantPersonas.getText()), 1, "Intercambio");
			} else {
				estadisticas.iniciarSimulacion(Integer.parseInt(cantPersonas.getText()), 1, "Donacion");
			}
			return;
		}
		
		
		if (intercambio.isSelected()) {
			estadisticas.iniciarSimulacion(Integer.parseInt(cantPersonas.getText()),
									Integer.parseInt(cantSimulaciones.getText()), "Intercambio");
		} else {
			estadisticas.iniciarSimulacion(Integer.parseInt(cantSimulaciones.getText()),
									Integer.parseInt(cantSimulaciones.getText()), "Donacion");
		}
		
	}
	
	private void mostrarConfiguraciones() {
		if (comboBox.getSelectedIndex() == 0) {
			lblPersonas.setVisible(false);
			cantPersonas.setVisible(false);
			lblSimulaciones.setVisible(false);
			cantSimulaciones.setVisible(false);
			preguntaRepetidas.setVisible(false);
			intercambio.setVisible(false);
			donaciones.setVisible(false);
			return;
		}
		if (comboBox.getSelectedIndex() == 1) {
			lblSimulaciones.setVisible(true);
			cantSimulaciones.setVisible(true);
			lblPersonas.setVisible(false);
			cantPersonas.setVisible(false);
			preguntaRepetidas.setVisible(false);
			intercambio.setVisible(false);
			donaciones.setVisible(false);
		}
		if (comboBox.getSelectedIndex() >= 2) {
			lblPersonas.setVisible(true);
			cantPersonas.setVisible(true);
			preguntaRepetidas.setVisible(true);
			intercambio.setVisible(true);
			donaciones.setVisible(true);
			lblSimulaciones.setVisible(false);
			cantSimulaciones.setVisible(false);
			if (comboBox.getSelectedIndex() == 3) {
				lblSimulaciones.setVisible(true);
				cantSimulaciones.setVisible(true);
			}
		} 
		btnSimular.setVisible(true);
	}

	@Override
	protected void ponerFondoDePantalla() {
		try {
			background.setBackground(ImageIO.read(new File("src/recursos/qatar2022-fondo-principal.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setContentPane(background);
		getContentPane().setLayout(null);
	}
}
