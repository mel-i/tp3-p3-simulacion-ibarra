package visual;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import controlador.Controlador;
import modelo.Persona;

public class SimulacionSW extends SwingWorker<List<Controlador>, Object>{
	private List<Controlador> simulaciones;
	private int cantPersonas;
	private int cantSimulaciones;
	private List<Persona> personas;
	private String tipoDeSimulacion;
	private JProgressBar barraProgreso;
	private JFreeChart grafico;
	private ChartPanel panelGrafico;
	
	public SimulacionSW(JProgressBar barra, ChartPanel panel, int cantDePersonas, 
							int cantDeSimulaciones, String tipoDeIntercambio) {
		simulaciones = new ArrayList<>();
		personas = new ArrayList<>();
		tipoDeSimulacion = tipoDeIntercambio;
		panelGrafico = panel;
		cantPersonas = cantDePersonas;
		cantSimulaciones = cantDeSimulaciones;
		barraProgreso = barra;
	}

	@Override
	protected List<Controlador> doInBackground() throws Exception {
		
		for (int i = 0; i < cantSimulaciones; i++) {
//			System.out.println("n de simulacion " + i);
			Controlador c = new Controlador();
			switch(tipoDeSimulacion) {
			case "Intercambio":
				c.simulacionConIntercambio(listaDePersonas());
				break;
			case "Donacion":
				c.simulacionConDonacion(listaDePersonas());
				break;
			default:
				c.simulacionSimple(new Persona("Participante"));
			}
			simulaciones.add(c);
		}
		
		mostrarGrafico();
		
		return simulaciones;
	}

	@Override
	public void done() {
		if (!this.isCancelled()) {
			panelGrafico.setChart(grafico);
			barraProgreso.setVisible(false);
			panelGrafico.setVisible(true);
		}
	}
	
	private void mostrarGrafico() {
		Controlador simulacion = simulaciones.get(0);
		if (cantPersonas == 1 && cantSimulaciones == 1) {
			grafico = Graficadora.graficoSimulacionSimple(simulacion.getPersonas().get(0));
			return;
		} 
		if (cantSimulaciones == 1 && cantPersonas > 1) {
			grafico = Graficadora.graficoSimulacion(simulacion.getPersonas());
			return;
		} 
		grafico = Graficadora.graficoSimulacionesVarias(simulaciones);
	}
	
	private List<Persona> listaDePersonas() {
		for (int i = 0; i < cantPersonas; i++) {
			personas.add(new Persona(i + ""));
		}
		return personas;
	}
	
}
