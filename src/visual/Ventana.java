package visual;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public abstract class Ventana extends JFrame {

	protected JPanel contentPane;
	protected Ventana siguienteVentana;
	protected JToggleButton btnSonido;
	protected boolean musica;
	private Long momentoDeLaCancion;
	private Clip musicaDeFondo;

	public Ventana() {
		momentoDeLaCancion = (long) 0;
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		musica = false;
	}
	
	protected abstract void crearFrame();

	protected void cargarMusica() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("src/recursos/Dreamers [World Cup Qatar 2022].wav"));
			musicaDeFondo = AudioSystem.getClip();
			musicaDeFondo.open(audioInputStream);
			musicaDeFondo.loop(Clip.LOOP_CONTINUOUSLY); 
		} catch (Exception ex) {
			System.out.println("Error with playing sound.");
			ex.printStackTrace();
		}
	}
	
	public boolean estadoMusica() {
		return musica;
	}
	
	protected void setMusica(boolean estadoDeMusica) {
		if (siguienteVentana != null && siguienteVentana.estadoMusica()) {
			siguienteVentana.musicaDeFondo.stop(); 
			siguienteVentana.musicaDeFondo.close();
			siguienteVentana.guardarEstadoDeSonido(estadoDeMusica);
		}
		if(estadoMusica()) {
			if (musicaDeFondo == null) {
				cargarMusica();
			}
			momentoDeLaCancion = musicaDeFondo.getMicrosecondPosition(); 
			musicaDeFondo.stop(); 
			musicaDeFondo.close();
			musica = false;
		} else {
			cargarMusica();
			musicaDeFondo.setMicrosecondPosition(momentoDeLaCancion); 
			musicaDeFondo.start();
			musica = true;
		}
	}
	
	protected void crearBotonSonido() {
		btnSonido = new JToggleButton("Sonido");
		btnSonido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				musica();
			}
		});

		btnSonido.setBounds(480, 10, 80, 30);
		getContentPane().add(btnSonido);
	}
	
	private void musica() {
		if (btnSonido.isSelected()) {	
			setMusica(false);
		} else {
			setMusica(true);
		}
	}
	
	public void guardarEstadoDeSonido(boolean estadoDeMusica) {
		btnSonido.setSelected(estadoDeMusica);
	}
	
	protected abstract void ponerFondoDePantalla();
	
	protected void actualizarPantalla() {
		validate();
		repaint();
	}

	protected void cambiarVentana() {
		siguienteVentana.setVisible(true);
		this.setVisible(false);;
	}
}
