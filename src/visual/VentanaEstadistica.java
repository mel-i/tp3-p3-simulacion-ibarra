package visual;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

import org.jfree.chart.ChartPanel;

import java.awt.Color;

public class VentanaEstadistica extends Ventana {

	private static final long serialVersionUID = 1L;
	private BackgroundPane background;
	private SimulacionSW simulacion;
	private ChartPanel grafico;
	private JProgressBar barraProgreso;

	public VentanaEstadistica() {
		super();
		background = new BackgroundPane();
		crearFrame();
	}

	@Override
	protected void crearFrame() {
		setTitle("Simulacion del Album");
		setBounds(100, 100, 600, 450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		ponerFondoDePantalla();
		
		grafico = new ChartPanel(null);
		grafico.setBounds(90, 110, 420, 245);
		getContentPane().add(grafico);
		grafico.setVisible(false);
		
		barraProgreso = new JProgressBar();
		barraProgreso.setForeground(new Color(128, 0, 0));
		barraProgreso.setBounds(69, 310, 460, 40);
		barraProgreso.setIndeterminate(true);
		getContentPane().add(barraProgreso);
		
	}
	
	public void iniciarSimulacion(Integer personas, Integer simulaciones, String tipoDeSimulacion) {
		simulacion = new SimulacionSW(barraProgreso, grafico, personas, simulaciones, tipoDeSimulacion);	
		simulacion.execute();
	}

	@Override
	protected void ponerFondoDePantalla() {
		try {
			background.setBackground(ImageIO.read(new File("src/recursos/qatar2022-fondo-estadisticas.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setContentPane(background);
		getContentPane().setLayout(null);
	}
}
