package visual;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import controlador.Controlador;
import modelo.Persona;

public class Graficadora {

	private static JFreeChart grafico;
	private static DefaultCategoryDataset datos;
	private List<Persona> personas;
	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Graficadora window = new Graficadora();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Graficadora() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		List<Controlador> simulaciones = new ArrayList<>();
		
		for (int i = 0; i < 24; i++) {
			Controlador c = new Controlador();
			c.simulacionConDonacion(listaDePersonas());
			simulaciones.add(c);
			System.out.println("n de simulacion " + i);
		}
		
		ChartPanel panel = new ChartPanel(graficoSimulacionesVarias(simulaciones));
		panel.setBounds(100, 100, 450, 300);
		frame.getContentPane().add(panel);
	}
	
	private List<Persona> listaDePersonas() {
		personas = new ArrayList<>();
		
		for (int i = 0; i < 20; i++) {
			personas.add(new Persona(i + ""));
		}
		return personas;
	}
	
				//// Simulacion de una persona ////
	
	public static JFreeChart graficoSimulacionSimple(Persona p) {
		grafico = ChartFactory.createBarChart(
				"Estadisticas",
				null,
				"Cantidades", 
				datosSimulacionSimple(p), 
				PlotOrientation.VERTICAL,
				true, false, false);
		
		return grafico;
	}
	
	private static DefaultCategoryDataset datosSimulacionSimple(Persona p) {
		datos = new DefaultCategoryDataset();
		datos.addValue(p.getCantDePaquetesComprados(), "Paquetes Comprados", p.getNombre());
		datos.addValue(p.getCantDeRepetidas(), "Repetidas", p.getNombre());
		
		return datos;
	}

				//// Unica Simulacion ////
	
	public static JFreeChart graficoSimulacion(List<Persona> simulacion) {
		grafico = ChartFactory.createBarChart(
				"Estadisticas", 
				"personas", 
				null, 
				datosDeSimulacion(simulacion), 
				PlotOrientation.VERTICAL, 
				true, false, false);
		
		return grafico;
	}
	
	private static DefaultCategoryDataset datosDeSimulacion(List<Persona> simulacion) {
		datos = new DefaultCategoryDataset();
		
		for (Persona p : simulacion) {
			datos.addValue(p.getCantDeDonaciones(), "Intercambios/Donaciones", p.getNombre());
			datos.addValue(p.getCantDePaquetesComprados(), "Paquetes Comprados", p.getNombre());
			datos.addValue(p.getCantDeRepetidas(), "Repetidas", p.getNombre());
		}
		
		return datos;
	}

				//// Varias Simulaciones ////
	
	public static JFreeChart graficoSimulacionesVarias(List<Controlador> simulaciones) {
		grafico = ChartFactory.createLineChart(
				"Estadisticas", 
				"Simulaciones", 
				"Datos Promedio", 
				datosDeSimulaciones(simulaciones), 
				PlotOrientation.VERTICAL, 
				true, false, false);
		
		return grafico;
	}
	
	private static DefaultCategoryDataset datosDeSimulaciones(List<Controlador> simulaciones) {
		datos = new DefaultCategoryDataset();
		
		for (int i = 0; i < simulaciones.size(); i++) {
			datos.addValue((Number)simulaciones.get(i).getIntercambiosPromedio(), "Intercambios", i);
			datos.addValue((Number)simulaciones.get(i).getPaquetesPromedio(), "Paquetes Comprados", i);
			datos.addValue((Number)simulaciones.get(i).getRepetidasPromedio(), "Repetidas", i);
		}
		
		return datos;
	}
}
