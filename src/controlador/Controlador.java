package controlador;

import java.util.List;

import modelo.GeneradorRandom;
import modelo.Persona;
import modelo.Simulacion;

public class Controlador {
	private Simulacion simulacion;

	public Controlador() {
		simulacion = new Simulacion(new GeneradorRandom());
	}
	
	public void simulacionSimple(Persona p) {
		simulacion.simular(p);
	}
	
	public void simulacionConDonacion(List<Persona> personas) {
		simulacion.simular(personas, p -> simulacion.donarRepetidas(p));
	}
	
	public void simulacionConIntercambio(List<Persona> personas) {
		simulacion.simular(personas, p -> simulacion.intercambiarRepetidas(p));
	}
	
	public List<Persona> getPersonas() {
		return simulacion.getPersonas();
	}
	
	public int getIntercambiosPromedio() {
		return simulacion.getIntercambiosPromedio();
	}

	public int getPaquetesPromedio() {
		return simulacion.getPaquetesPromedio();
	}

	public int getRepetidasPromedio() {
		return simulacion.getRepetidasPromedio();
	}
}
