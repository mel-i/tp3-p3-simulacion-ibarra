package modelo;

import java.util.ArrayList;
import java.util.List;

public class SimulacionTest {
	private static Simulacion s;

	public static void main(String[] args) {
		Generador generador = new GeneradorRandom();
		s = new Simulacion(generador);
		s.guardarObservador(new ObservadorPorConsola(s));
		
		List<Persona> personas = new ArrayList<>();
		personas.add(new Persona("Amanda"));
		personas.add(new Persona("Patricio"));
		personas.add(new Persona("Guido"));
		personas.add(new Persona("Gaston"));
		
//		simularConUnaPersona(new Persona("Guido"));
		
//		simularConDonaciones(personas);
		
		simularConIntercambios(personas);
	}
	
	public static void simularConUnaPersona(Persona p) {
		s.simular(p);
	}
	
	public static void simularConDonaciones(List<Persona> personas) {
		s.simular(personas, p -> s.donarRepetidas(p));
	}
	
	public static void simularConIntercambios(List<Persona> personas) {
		s.simular(personas, p -> s.intercambiarRepetidas(p));
	}
}
