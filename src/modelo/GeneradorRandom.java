package modelo;

import java.util.Random;

public class GeneradorRandom implements Generador {
	private Random random;
	private int rango;
	private int[] paquete;
	
	public GeneradorRandom() {
		random = new Random();
		rango = 638;
		paquete = new int[5];
	}

	@Override
	public int[] getPaqueteFigus() {
		for (int i = 0; i < 5; i++) {
			paquete[i] = random.nextInt(rango);
		}
		return paquete;
	}
}
