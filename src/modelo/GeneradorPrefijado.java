package modelo;

public class GeneradorPrefijado implements Generador {
	private int repetidas;
	private int figu;
	private int[] paquete;
	
	public GeneradorPrefijado() {
		repetidas = 0;
		figu = 0;
		paquete = new int[5];
	}

	@Override
	public int[] getPaqueteFigus() {
		for (int i = 0; i < 5; i++) {
			paquete[i] = figu;
			actualizarRepetidas();
		}
		return paquete;
	}

	private void actualizarRepetidas() {
		if (repetidas == 0) {
			actualizarFigurita();
		} else {
			repetidas--;
		}
	}
	
	private void actualizarFigurita() {
		if (figu == 637) {
			figu = 0;
		} else {
			figu++;
		}
	}

	public void setRepetidas(int cantDeRepetidas) {
		repetidas = cantDeRepetidas;
	}
	
	public void setFigu(int figurita) {
		figu = figurita;
	}
}
