package modelo;

public class Persona implements Comparable<Persona> {
	private String nombre;
	private boolean[] album;
	private int[] repetidas;
	private int paquetesComprados;
	private int figusPegadas;
	private int figusRepetidas;
	private int figusTotales;
	private int precioDelAlbum;
	private int precioXPaquete;
	private int intervenciones;
	
	public Persona(String nombre) {
		this.nombre = nombre;
		figusTotales = 638;
		figusPegadas = 0;
		figusRepetidas = 0;
		paquetesComprados = 0;
		album = new boolean[figusTotales];
		repetidas = new int[figusTotales];
		precioDelAlbum = 700;
		precioXPaquete = 150;
		intervenciones = 0;
	}
	
	public void pegarFigus(int[] paqueteFigus) {
		int figu = 0;
		for (int i = 0; i < paqueteFigus.length; i++) {
			figu = paqueteFigus[i];
			if (!album[figu]) {
				album[figu] = true;
				figusPegadas++;
			} else {
				repetidas[figu]++;
				figusRepetidas++;
			}
		}
		paquetesComprados++;
	}
	
	public void intercanbiar(int figuOfrecida, int figuRecibida) {
		dar(figuOfrecida);
		recibir(figuRecibida);
	}
	
	public void dar(int figu) {
		repetidas[figu]--;
		figusRepetidas--;
		intervenciones++;
	}
	
	public void recibir(int figu) {
		album[figu] = true;
		figusPegadas++;
	}
	
	public int getCantDePaquetesAComprar() {
		int faltantes = figusTotales - figusPegadas;
		if (faltantes == 0) {
			return 0;
		}
		if (faltantes < 5) {
			return 2;
		}
		if (faltantes < 20) {
			return 5;
		}
		if (faltantes < 50) {
			return 10;
		}
		return 20;
	}
	
	public boolean tieneFiguRepetida(int figu) {
		return repetidas[figu] > 0;
	}

	public boolean tieneFigu(int figu) {
		return album[figu];
	}
	
	public int getCantDeVecesRepetida(int figu) {
		return repetidas[figu];
	}
	
	public int getCantDePaquetesComprados() {
		return paquetesComprados;
	}
	
	public int getCantDeFigus() {
		return figusPegadas;
	}
	
	public int getCantDeRepetidas() {
		return figusRepetidas;
	}

	public boolean tieneAlbumCompleto() {
		return figusPegadas == figusTotales;
	}
	
	public int getGasto() {
		return precioDelAlbum + precioXPaquete*paquetesComprados;
	}
	
	public int getCantDeDonaciones() {
		return intervenciones;
	}
	
	public String getNombre() {
		return nombre;
	}

	@Override
	public int compareTo(Persona o) {
		return - this.figusPegadas + o.figusPegadas;
	}
	
	@Override
	public String toString() {
		return (nombre + ": figus: " + figusPegadas + 
				" figus repetidas: " + figusRepetidas +
			" paquetesComprados: " + paquetesComprados);
	}
}
