package modelo;

public class ObservadorPorConsola implements Observador {

	private Simulacion simulacion;
	
	public ObservadorPorConsola(Simulacion s) {
		simulacion = s;
	}
	
	@Override
	public void notificar() {
		System.out.println("Datos Parciales");
		for (Persona p : simulacion.getPersonas()) {
			System.out.println(p.toString());
		}
		System.out.println();
	}
}
