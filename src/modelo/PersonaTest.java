package modelo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonaTest {
	private Persona persona;
	private GeneradorPrefijado generador;

	@Before
	public void inicializar() {
		persona = new Persona("Amanda");
		generador = new GeneradorPrefijado();
	}
	
	@Test
	public void pegarFigusTest() {
		persona.pegarFigus(generador.getPaqueteFigus());
		
		assertEquals(5, persona.getCantDeFigus());
	}
	
	@Test
	public void pegarFigusRepetidasTest() {
		generador.setRepetidas(1);
		persona.pegarFigus(generador.getPaqueteFigus());
		
		assertEquals(4, persona.getCantDeFigus());
	}
	
	@Test
	public void pegar2PaquetesSinRepetidasTest() {
		persona.pegarFigus(generador.getPaqueteFigus());
		persona.pegarFigus(generador.getPaqueteFigus());
		
		assertEquals(10, persona.getCantDeFigus());
	}
	
	@Test
	public void pegar2PaquetesConRepetidasTest() {
		generador.setRepetidas(3);
		persona.pegarFigus(generador.getPaqueteFigus());
		generador.setRepetidas(1);
		persona.pegarFigus(generador.getPaqueteFigus());
		
		assertEquals(6, persona.getCantDeFigus());
	}
	
	@Test
	public void albumSinRepetidasTest() {
		for (int i = 0; i < 128; i++) {
			persona.pegarFigus(generador.getPaqueteFigus());
		}
		
		assertTrue(persona.tieneAlbumCompleto());
		assertEquals(2, persona.getCantDeRepetidas());
	}
}
