package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class Simulacion {
	private List<Persona> personas;
	private Generador kiosco;
	private Observador observador;
	
	public Simulacion(Generador generador) {
		personas = new ArrayList<>();
		kiosco = generador;
	}

	public void guardarObservador(Observador o) {
		observador = o;
	}
	
	public void simular(Persona p) {
		personas.add(p);
		
		while(!albumesCompletos()) {
			llenarAlbumes();
			notificar();
		}
	}
	
	public void simular(List<Persona> instancia, Consumer<Persona> metodoDeIntercambio) {
		personas = instancia;
		
		while(!albumesCompletos()) {
			llenarAlbumes();
			repartirRepetidas(metodoDeIntercambio);
			notificar();
		}
	}

	private boolean albumesCompletos() {
		boolean ret = true;
		for(Persona p : personas) {
			ret = ret && p.tieneAlbumCompleto();
		}
		return ret;
	}
	
	private void llenarAlbumes() {
		for (Persona p : personas) {
			comprarFigus(p);
		}
	}
	
	private void comprarFigus(Persona p) {
		for (int i = 0; i < p.getCantDePaquetesAComprar(); i++) {
			p.pegarFigus(kiosco.getPaqueteFigus());
		}
	}
	
	private void repartirRepetidas(Consumer<Persona> metodoDeIntercambio) {
		Collections.sort(personas, (p, q) -> -p.getCantDeRepetidas() + q.getCantDeRepetidas());
		
		for (Persona p : personas) {
			metodoDeIntercambio.accept(p);
		}
	}
	
	//////// Donacion de figuritas ////////
	
	public void donarRepetidas(Persona p) {
		if (p.getCantDeRepetidas() == 0) {
			return;
		}
		
		for (int figu = 0; figu < 638; figu++) {
			if (p.tieneFiguRepetida(figu)) {
				buscarAQuienDonar(p, figu);
			}
		}
	}
	
	private void buscarAQuienDonar(Persona p, int figurita) {	
		for (Persona otro : personas) {
			if (!otro.tieneFigu(figurita) && p.getCantDeVecesRepetida(figurita) > 0) {
				p.dar(figurita);
				otro.recibir(figurita);
			}
		}
	}
	
	//////// Intercambio de figuritas ////////
	
	public void intercambiarRepetidas(Persona p) {
		if (p.getCantDeRepetidas() == 0) {
			return;
		}
		
		for (int figu = 0; figu < 638; figu++) {
			if (p.tieneFiguRepetida(figu)) {
				buscarIntercambio(p, figu);
			}
		}
	}
	
	private void buscarIntercambio(Persona ofrecedor, int figu) {
		for (Persona otro : personas) {
			if (!otro.tieneFigu(figu) && ofrecedor.getCantDeVecesRepetida(figu) > 0) {
				buscarFiguDeIntercambio(ofrecedor, figu, otro);
			}
		}
	}
	
	private void buscarFiguDeIntercambio(Persona ofrecedor, int figuOfrecida, Persona receptor) {
		for (int figu = 0; figu < 638; figu++) {
			if (receptor.tieneFiguRepetida(figu) && !ofrecedor.tieneFigu(figu)) {
				ofrecedor.intercanbiar(figuOfrecida, figu);
				receptor.intercanbiar(figu, figuOfrecida);
				return;
			}
		}
	}
	
	private void notificar() {
		if (observador != null) {
			observador.notificar();
		}
	}
	
	public List<Persona> getPersonas() {
		return personas;
	}

	public int getIntercambiosPromedio() {
		int promedio = 0;
		for (Persona p : personas) {
			promedio += p.getCantDeDonaciones();
		}
		return promedio / personas.size();
	}

	public int getPaquetesPromedio() {
		int promedio = 0;
		for (Persona p : personas) {
			promedio += p.getCantDePaquetesComprados();
		}
		return promedio / personas.size();
	}

	public int getRepetidasPromedio() {
		int promedio = 0;
		for (Persona p : personas) {
			promedio += p.getCantDeRepetidas();
		}
		return promedio / personas.size();
	}
}
