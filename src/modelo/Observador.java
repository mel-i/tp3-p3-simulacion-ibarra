package modelo;

public interface Observador {
	public void notificar();
}
